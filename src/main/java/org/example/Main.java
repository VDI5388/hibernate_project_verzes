package org.example;

import org.example.dao.AddressDao;
import org.example.dao.PersonDao;
import org.example.entities.Address;
import org.example.entities.AddressId;
import org.example.entities.BuildingType;
import org.example.entities.Person;

import java.sql.SQLOutput;
import java.util.HashMap;
import java.util.Map;

public class Main {

    private static AddressDao addressDao = new AddressDao();
    private static PersonDao personDao = new PersonDao();

    public static void main(String[] args) {

        AddressId addressId = new AddressId("Primaverii", 14, "Cluj-Napoca");
        AddressId addressId1 = new AddressId("Joseph Haydn", 10, "Timisoara");
        AddressId addressId2 = new AddressId("Crisurilor", 75, "Timisoara");
        AddressId addressId3 = new AddressId("Siemens", 1, "Timisoara");



        Address address = new Address(addressId, 150, BuildingType.PRIVATE_HOUSE);
        addressDao.createAddress(address);

        Address address1 = new Address(addressId1, 84, BuildingType.DUPLEX);
        addressDao.createAddress(address1);

        Address address2 = new Address(addressId2, 250, BuildingType.PRIVATE_HOUSE);
        addressDao.createAddress(address2);

        Address address3 = new Address(addressId3, 10000, BuildingType.OFFICE);
        addressDao.createAddress(address3);


        Person firstPerson= new Person("1860718303709" , "Verzes Ionut-Daniel", 1986, address);
        personDao.createPerson(firstPerson);

        Address a1 =  addressDao.readAddress(addressId);

        System.out.println(a1);

        System.out.println(personDao.readPerson("1860718303709"));

        Person p1 = new Person("193040567842345", "Ion", 1993, address);
        Person p2 = new Person("177040567842345", "Vlad", 1977, address1);
        Person p3 = new Person("186040567842345", "Mihai", 1986, address);
        Person p4 = new Person("168040567842345", "Dorin", 1968, address2);
        Person p5 = new Person("289121298983556", "Maria", 1989, address);
        Person p6 = new Person("299121298983556", "Ioana", 1999, address3);
        Person p7 = new Person("278121298983556", "Simona", 1978, address3);

        personDao.createPerson(p1);
        personDao.createPerson(p2);
        personDao.createPerson(p3);
        personDao.createPerson(p4);
        personDao.createPerson(p5);
        personDao.createPerson(p6);
        personDao.createPerson(p7);

        p3.setAddress(address3);
        personDao.updatePerson(p3);

        personDao.readAllPerson().forEach(p-> System.out.println(p));

        personDao.readAllPerson().forEach((p) -> {
            prettyDisplayOfPerson(p);
        });


        p7.setName("Irina");
        personDao.updatePerson(p7);

        personDao.removePerson(p7);

        System.out.println("-------------------");

        personDao.readAllPerson().forEach((p) -> {
            prettyDisplayOfPerson(p);
        });

        System.out.println("Young persons");
        personDao.youngerThan(1990).forEach(System.out::println);


        System.out.println("-----------------------------");

        personDao.allFrom("Cluj-Napoca").forEach(System.out::println);

        /*// v1
        //personDao.readAllPerson().forEach((p) -> {
        //    prettyDisplayOfPerson(p);
        //});

        // v2
        //personDao.readAllPerson().forEach(p -> prettyDisplayOfPerson(p));

        // v3
        //personDao.readAllPerson().forEach(Main::prettyDisplayOfPerson);


        Map<Integer, String> numbers = new HashMap<>();
        numbers.put(11, "unsprezece");
        numbers.put(100, "o sută");
        numbers.put(-2, "minus doi");

        numbers.forEach((k, v) -> {
            System.out.println("Numărul " + k + " se pronunță " + v);
        });

        numbers.forEach((k,v) -> prettyPrint(k,v));

        numbers.forEach(Main::prettyPrint);*/

    }

    static void prettyDisplayOfPerson(Person p) {
        System.out.println(
                p.getName() +
                        " născut în " + p.getYearOfBirth() +
                        " locuiește în " + p.getAddress().getId().getCity());
    }


    static void prettyPrint(Integer nr, String traducere) {
        System.out.println("+++ Numărul " + nr + " se pronunță " + traducere + " ++++");

    }


}